using System;
using Xunit;
using PatientBookingApp;
using static XUnitTestProject1.CheckValidation;
using System.Linq;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void TestBookModel()
        {

            CheckPropertyValidation test = new CheckPropertyValidation();
            var book = new Book
            {
                BookedDate = DateTime.Parse("10-10-2010"),
                TimeSlotId = 1,
                TreatmentId = 26,
                Email = "tariqahmad1987@gmail.com",
                Name = "Tariq",
                Mobile = "0798482345",
                Note = "There is no notes",
                IsDeleted = false


            };
            var errorcount = test.myValidation(book).Count();
            Assert.Equal(0, errorcount);


        }
        [Fact]
        public void TestBookModelInavlidEmail()
        {

            CheckPropertyValidation test = new CheckPropertyValidation();
            var book = new Book
            {
                BookedDate = DateTime.Parse("10-10-2010"),
                TimeSlotId = 1,
                TreatmentId = 26,
                Email = "tariqahmad1987gmail.com",
                Name = "Tariq",
                Mobile = "0798482345",
                Note = "There is no notes",
                IsDeleted = false


            };
            bool IsContainsAt = book.Email.Contains("@");
            var errorcount = test.myValidation(book).Count();
            Assert.Equal(0, errorcount);
            Assert.True(IsContainsAt);


        }
        [Fact]
        public void TestTimeSlotModel()
        {

            CheckPropertyValidation test = new CheckPropertyValidation();
            var timeSlot = new TimeSlot
            {
                TimeSlotTitle = 1,

            };
            var errorcount = test.myValidation(timeSlot).Count();
            Assert.Equal(0, errorcount);
        }
        [Fact]
        public void TreatmentSlotModel()
        {

            CheckPropertyValidation test = new CheckPropertyValidation();
            var treatment = new Treatment
            {
                TreatmentDescription = "Description",
                TreatmentTitle = "Some ting"

            };
            var errorcount = test.myValidation(treatment).Count();
            Assert.Equal(0, errorcount);
        }
        [Fact]
        public void AddTest()
        {
            var add = new Book() { };

            
        }
    }
}
