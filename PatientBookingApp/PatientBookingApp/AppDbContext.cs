﻿
using Microsoft.EntityFrameworkCore;
namespace PatientBookingApp
{
    
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }
        public DbSet<Book> Books { get; set; }
        public DbSet<TimeSlot> TimeSlots { get; set; }
        public DbSet<Treatment> Treatments { get; set; }


    }
}

