﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace PatientBookingApp
{
    [Table("Bookings")]
    public class Book
    {
        [Required, Key]
        public int BookingId { get; set; }
        [ Required, StringLength(50), MinLength(3)]
        public string Name { get; set; }
        [Required, StringLength(50), MinLength(10)]
        public string Mobile { get; set; }
        [DataType(DataType.Date)]
        public DateTime BookedDate { get; set; }
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        public object myValidation(Book book)
        {
            throw new NotImplementedException();
        }

        [StringLength(maximumLength:100000)]
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
        public int TimeSlotId { get; set; }
        public int TreatmentId { get; set; }
        public TimeSlot  TimeSlot { get; set; }
        public Treatment Treatment { get; set; }



    }


    [Table("LU_TimeSlots")]
    public class TimeSlot
    {
        [Key]
        public int Id { get; set; }
        public int TimeSlotTitle { get; set; }
        public ICollection<Book> Books { get; set; }

    }


    [Table("LU_Tratment")]
    public class Treatment
    {
        [Key]
        public int Id { get; set; }
        public string TreatmentTitle { get; set; }
        public string TreatmentDescription { get; set; }
        public ICollection<Book> Books { get; set; }


    }
}