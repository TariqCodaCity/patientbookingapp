﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace PatientBookingApp.Pages
{
    public class AddBookModel : PageModel
    {
        private readonly AppDbContext _db;
        public AddBookModel(AppDbContext db)
        {
            _db = db;
        }

        [TempData]
        public string Message { get; set; }

        [BindProperty]
        public TimeSlot TimeSlot { get; set; }

        [BindProperty]
        public Treatment Treatment { get; set; }


        [BindProperty]
        public Book Book { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

                    _db.TimeSlots.Add(TimeSlot);
                    _db.Treatments.Add(Treatment);
                    Book.TimeSlotId = TimeSlot.Id;
                    Book.TreatmentId = Treatment.Id;
                    _db.Books.Add(Book);
                    await _db.SaveChangesAsync();
                    Message = $"{Book} Added sucessfully!";
                    return RedirectToPage("/Index");

        }

    }
}