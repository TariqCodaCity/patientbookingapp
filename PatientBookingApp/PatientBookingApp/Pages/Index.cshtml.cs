﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace PatientBookingApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _db;
        public IndexModel(AppDbContext db)
        {
            _db = db;
        }

        public IList<Book> Books { get; private set; }
        public IList<Treatment> Treatments { get; private set; }
        public IList<TimeSlot> TimeSlots { get; private set; }

        public string Message { get; set; }
          public async Task OnGetAsync(string searchString)
          {

              var books = from m in _db.Books
                           select m;
              books = books.Where(g => g.IsDeleted.Equals(false));
              if (!String.IsNullOrEmpty(searchString))
              {
                  books = books.Where(s => s.Name.Contains(searchString) || s.Mobile.Contains(searchString));
              }



              Books = await books.ToListAsync();











            //  Books = await _db.Books.AsNoTracking().ToListAsync();

          }


        public async Task<IActionResult> OnPostDeleteAsync(int id)
        {
            var book = await _db.Books.FindAsync(id);
            if(book != null)
            {
                book.IsDeleted = true;
            
                await _db.SaveChangesAsync();
            }
            return RedirectToPage();

        }

    }
}
