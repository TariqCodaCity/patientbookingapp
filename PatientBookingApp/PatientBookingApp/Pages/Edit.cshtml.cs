﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace PatientBookingApp.Pages
{
    public class EditModel : PageModel
    {
        private readonly AppDbContext _db;
        public EditModel(AppDbContext db)
        {
            _db = db;
        }
        [BindProperty]
        public TimeSlot TimeSlot { get; set; }
        [BindProperty]
        public Treatment Treatment { get; set; }
        [BindProperty]
        public Book Book { get; set; }
        public async Task<IActionResult> OnGetAsync(int id)
        {
            Book = await _db.Books.FindAsync(id);
            if(Book == null)
            {
                RedirectToPage("/Index");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _db.Attach(TimeSlot).State = EntityState.Modified;
            _db.Attach(Treatment).State = EntityState.Modified;

            _db.Attach(Book).State = EntityState.Modified;
            try
            {

               
                await _db.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                throw new Exception($"Book {Book.BookingId} not Found!");
            }
            return RedirectToPage("./Index");
        }

    }
}